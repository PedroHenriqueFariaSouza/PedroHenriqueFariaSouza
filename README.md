# Hello, learn a little about me.
Pedro Henrique F. Souza, Brazilian, 29yrs.

> Web Developer (front-end and back-end).

### Some Technologies

![C](https://img.shields.io/badge/C-239120?style=for-the-badge&logo=c&logoColor=white) 
![Apache](https://img.shields.io/badge/Apache-CA2136?style=for-the-badge&logo=apache&logoColor=white)
![Nginx](https://img.shields.io/badge/Nginx-009639?style=for-the-badge&logo=nginx&logoColor=white)
![Node-Js](https://img.shields.io/badge/Node.js-43853D?style=for-the-badge&logo=node.js&logoColor=white)
![html5](https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white)
![Css](https://img.shields.io/badge/CSS-239120?&style=for-the-badge&logo=css3&logoColor=white)
![Saas](https://img.shields.io/badge/Sass-CC6699?style=for-the-badge&logo=sass&logoColor=white)
![Bootstrap](https://img.shields.io/badge/Bootstrap-563D7C?style=for-the-badge&logo=bootstrap&logoColor=white)
![MySql](https://img.shields.io/badge/MySql-00000F?style=for-the-badge&logo=mysql&logoColor=white)
![Javascript](https://img.shields.io/badge/JavaScript-323330?style=for-the-badge&logo=javascript&logoColor=F7DF1E)
![Python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![Jquery](https://img.shields.io/badge/jQuery-0769AD?style=for-the-badge&logo=jquery&logoColor=white)
![Java](https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white)
![ExpressJs](https://img.shields.io/badge/expressJs-323330?style=for-the-badge&logo=express&logoColor=white)
![Nodemailer](https://img.shields.io/badge/nodemailer-330F63?style=for-the-badge&logo=nodemailer&logoColor=white)
![NodePackageManagerNpm](https://img.shields.io/badge/npm-FF0000?style=for-the-badge&logo=npm&logoColor=white)
![PackageInstallerForPythonPip](https://img.shields.io/badge/pip-00599C?style=for-the-badge&logo=pip&logoColor=white)
![GNU BASH](https://img.shields.io/badge/GNU%20Bash-4EAA25?style=for-the-badge&logo=GNU%20Bash&logoColor=white)


### Tools

![Vscode](https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white)
![git](https://img.shields.io/badge/Git-FF0000?style=for-the-badge&logo=git&logoColor=white)
![LinuxUbuntu](https://img.shields.io/badge/ubuntu-DD4814?style=for-the-badge&logo=ubuntu&logoColor=white)


### DevOps

![DigitalOcean](https://img.shields.io/badge/DigitalOcean-0080FF?style=for-the-badge&logo=digitalocean&logoColor=white)
![Amazon Web Services](https://img.shields.io/badge/Amazon_AWS-232F3E?style=for-the-badge&logo=amazon-aws&logoColor=white)



### Contact

<a href="https://gitlab.com/PedroHenriqueFariaSouza">![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)</a>
<a href="https://www.linkedin.com/in/pedro-henrique-faria-souza/">![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)</a>
<a href="demoniokeynow@gmail.com">![gmail](https://img.shields.io/badge/Gmail-FF0000?style=for-the-badge&logo=gmail&logoColor=white) </a>
<a href="https://www.instagram.com/fariasouzapedrohenrique/">![Instagram](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white
)</a>
<a href="pedrohenriquefariasouza@hotmail.com">![Hotmail](https://img.shields.io/badge/Microsoft_Outlook-0078D4?style=for-the-badge&logo=microsoft-outlook&logoColor=white)</a>



